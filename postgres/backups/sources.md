# Postgres Backup Sources

* [pgBackRest User Guide \- Debian \& Ubuntu](https://pgbackrest.org/user-guide.html "pgBackRest User Guide - Debian \& Ubuntu")
* [pgBackRest User Guide \- Debian \& Ubuntu](https://pgbackrest.org/user-guide.html#monitor "pgBackRest User Guide - Debian \& Ubuntu")
* [woblerr\/docker\-pgbackrest\: pgBackRest inside Docker\.](https://github.com/woblerr/docker-pgbackrest "woblerr/docker-pgbackrest: pgBackRest inside Docker.")
* [Introducing pgBackRest Multiple Repository Support](https://www.crunchydata.com/blog/introducing-pgbackrest-multiple-repository-support "Introducing pgBackRest Multiple Repository Support")
* [pgBackRest – Running Backup from a Standby Server \| MigOps](https://www.migops.com/blog/pgbackrest-running-backup-from-a-standby-server/ "pgBackRest – Running Backup from a Standby Server | MigOps")
* [pgstef\/check\_pgbackrest\: pgBackRest backup check plugin for Nagios](https://github.com/pgstef/check_pgbackrest "pgstef/check_pgbackrest: pgBackRest backup check plugin for Nagios")
* [michaloo\/go\-cron\: Simple golang wrapper over \`github\.com\/robfig\/cron\` and \`os\/exec\` as a cron replacement](https://github.com/michaloo/go-cron "michaloo/go-cron: Simple golang wrapper over `github.com/robfig/cron` and `os/exec` as a cron replacement")
* [docker\-postgres\-backup\-local\/backup\.sh at main · prodrigestivill\/docker\-postgres\-backup\-local](https://github.com/prodrigestivill/docker-postgres-backup-local/blob/main/backup.sh "docker-postgres-backup-local/backup.sh at main · prodrigestivill/docker-postgres-backup-local")
* [Best practices for cron \| End Point Dev](https://www.endpointdev.com/blog/2008/12/best-practices-for-cron/ "Best practices for cron | End Point Dev")
